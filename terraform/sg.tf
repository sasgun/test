resource "aws_security_group" "ssh" {
  name = "ec2_sg"
  vpc_id      = aws_vpc.ec2_vpc.id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
  
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }


  tags = {
    Name = "ssh_sg"
  }

    depends_on = [aws_vpc.ec2_vpc,
                ]

}
resource "aws_security_group" "http_sg" {
  name = "http_sg"
  vpc_id      = aws_vpc.ec2_vpc.id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
  tags = {
    Name = "http_sg"
  }

    depends_on = [aws_vpc.ec2_vpc,
                ]
}
resource "aws_security_group" "https_sg" {
  name = "https_sg"
  vpc_id      = aws_vpc.ec2_vpc.id

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
  tags = {
    Name = "https_sg"
  }

    depends_on = [aws_vpc.ec2_vpc,
                ]
}

