resource "aws_instance" "ec2_instance" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.ec2_subnet.id
  key_name = aws_key_pair.sasga.id 
  vpc_security_group_ids = [aws_security_group.ssh.id,
			   aws_security_group.http_sg.id
			   ]
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  associate_public_ip_address = true
  depends_on = [aws_subnet.ec2_subnet,
		aws_key_pair.sasga,
                aws_security_group.ssh
		]
  tags = {
    Name = "ec2_sasga"  
  }
}

