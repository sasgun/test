resource "aws_vpc" "ec2_vpc" {
  cidr_block = "10.10.0.0/16"
  
  tags = {
    owner = "Sasga"
  }
}

resource "aws_subnet" "ec2_subnet" {
  vpc_id            = aws_vpc.ec2_vpc.id
  cidr_block        = "10.10.1.0/24"
  availability_zone = var.zone 

  tags = {
    owner = "Sasga",
    Name = "ec2_subnet"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.ec2_vpc.id

  tags = {
    owner = "Sasga",
    Name = "ec2_gw"
  }
}


resource "aws_route_table" "ec2_route" {
  vpc_id = aws_vpc.ec2_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }


  tags = {
    Name = "ec2_route"
  }
}

resource "aws_route_table_association" "ec2_route_add" {
  subnet_id      = aws_subnet.ec2_subnet.id
  route_table_id = aws_route_table.ec2_route.id
}
