resource "aws_lb_target_group" "ec2-target-group" {
  name     = "ec2-target-group"
  port     = 80
  protocol = "TCP"
  vpc_id   = aws_vpc.ec2_vpc.id
}

resource "aws_lb_target_group_attachment" "ec2-tg-attach" {
  target_group_arn = aws_lb_target_group.ec2-target-group.arn
  target_id        = aws_instance.ec2_instance.id
  port             = 80
}
resource "aws_lb" "ec2_elb" {
  name               = "ec2-elb"
  internal           = false
  load_balancer_type = "network"
#  security_groups    = [aws_security_group.http_sg.id,aws_security_group.https_sg.id]
  subnets            = [aws_subnet.ec2_subnet.id]

#  enable_deletion_protection = true

#  access_logs {
#    bucket  = aws_s3_bucket.lb_logs.bucket
#    prefix  = "test-lb"
#    enabled = true
#  }

  tags = {
    Environment = "ec2_elb",
    owner = "Sasga"
  }
}

resource "aws_lb_listener" "ec2_front_https" {
  load_balancer_arn = aws_lb.ec2_elb.arn
  port              = "443"
  protocol          = "TLS"
  certificate_arn   = "arn:aws:acm:eu-central-1:043288435865:certificate/117f9178-77d3-4990-af3a-7c162f7764ba"
  alpn_policy       = "HTTP2Optional"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ec2-target-group.arn
  }
}

resource "aws_lb_listener" "ec2_front_http" {
  load_balancer_arn = aws_lb.ec2_elb.arn
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.ec2-target-group.arn
  }
}

