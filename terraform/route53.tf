data "aws_route53_zone" "ashamans_zone" {
  name         = "dev.ashamans.ru"
  private_zone = false
}




resource "aws_route53_record" "domain" {
  zone_id = data.aws_route53_zone.ashamans_zone.zone_id
  name    = "dev.ashamans.ru"
  type    = "A"

  alias {
    name                   = aws_lb.ec2_elb.dns_name
    zone_id                = aws_lb.ec2_elb.zone_id
    evaluate_target_health = true
  }
    depends_on = [
    aws_lb.ec2_elb
  ]

}
