terraform {
  backend "s3" {
    bucket = "terraform-bucket4"
    key    = "terraform"
    region = "eu-central-1"
  }
}
