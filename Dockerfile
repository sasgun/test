FROM nginx:1.23.1

COPY nginx/conf/default /etc/nginx/sites-available/default

COPY files/ /usr/share/nginx/html/

EXPOSE 80/tcp

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
